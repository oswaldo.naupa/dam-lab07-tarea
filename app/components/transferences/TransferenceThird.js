var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button,TouchableOpacity,Dimensions,StyleSheet,ScrollView,Image} from 'react-native';
const { width } = Dimensions.get("window");
const ancho = width -120;
export default class TransferenceThird extends Component {
    constructor(props){
        super(props);
        this.state = {
            importe: 0,
        };
    }

    render() {
        return(
          
            <View style={styles.container}>
              <ScrollView>
              <View style={styles.colum}>
                <Image style={styles.imageView} source={require('./img/img.png')}/>
                <View style={styles.button}>
                    <TouchableOpacity style={styles.buttoncolor} onPress={()=>this.props.navigation.navigate('First')}> 
                        <Text style={styles.textbuton}> ACEPTAR </Text>  
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginBottom: 5,
      alignItems: 'center',
      flex:1
    },
    colum: {
      flexDirection:'column', alignSelf:'stretch',justifyContent: 'center',
    },
    button: {
      width: ancho*0.5,
      alignSelf: "center",
      margin:10
      
    },
    buttoncolor:{
        backgroundColor:'#000080',
        height: 40,
        borderRadius: 6,
        flexDirection: 'row',alignItems: 'center' 
      },
      textbuton:{
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 15,
        textAlignVertical: "center",
        width: '100%',
        color: '#ffffff'
      },
    containerboton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
      },
    touch:{
      alignSelf: "center",
      height: 40,
      borderRadius: 6, 
      backgroundColor: '#dcdcdc',
      width: ancho*0.3,
      margin:15,
      flexDirection: 'row',alignItems: 'center'
    },
    input:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'#dcdcdc',
    },
    inputmodal:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      backgroundColor:'#dcdcdc',
      textAlign: 'center',
      fontSize: 15,
    },

    inputref:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'red',
      
    },
    date:{
      textAlign: 'center', // <-- the magic
      fontWeight: 'bold',
      fontSize: 15,
      textAlignVertical: "center",
      width: '100%'
    },
    text:{
      fontSize: 15,
      margin:4,
      width:ancho,
    },
    textresult:{
        fontSize: 20,
        margin:4,
        width:ancho,
        borderBottomColor:'red'
    },
    separador:{
        height: 2, 
        width: "100%",
        backgroundColor: "#a9a9a9",
    },
    imageView:{
        height:210,
        width:210, 
        alignSelf: "center",
        margin:10
    }
  })